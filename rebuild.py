#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Author: lonewolf
# Date: 2013-11-11 23:08:52
# 
import os
import re
import codecs
try:
    import helper
except ImportError:
    from . import helper

snippetTemplate = """<snippet>
    <content><![CDATA[$content]]></content>
    <tabTrigger>$trigger</tabTrigger>
    <scope>source.python</scope>
    <description>$desc</description>
</snippet>
"""

# user definition path: SAVE_DIR/user_definition.json
USER_DEFINITIONS=[]
# auto completions path: SAVE_DIR/md5(filePath)/...
SAVE_DIR=""

def rebuild(dir,saveDir):
    global USER_DEFINITIONS
    global SAVE_DIR
    USER_DEFINITIONS=[]
    SAVE_DIR=saveDir
    # delete files first
    deleteFiles(saveDir,saveDir)
    parseDir(dir)
    return USER_DEFINITIONS

def rebuildSingle(filePath,saveDir):
    global USER_DEFINITIONS
    global SAVE_DIR
    USER_DEFINITIONS=[]
    SAVE_DIR=saveDir
    parseFile(filePath)
    return [USER_DEFINITIONS,filePath]

def parseDir(dir):
    for item in os.listdir(dir):
        path=os.path.join(dir,item)
        if os.path.isdir(path):
            parseDir(path)
        elif os.path.isfile(path):
            if helper.checkFileExt(path,"py"):
                parseFile(path)

def parseFile(filePath):
    # remove all file
    md5filename=helper.md5(filePath)
    saveDir=os.path.join(SAVE_DIR,md5filename)
    deleteFiles(saveDir,saveDir)
    # create dir
    if not os.path.exists(saveDir):
        os.makedirs(saveDir)
    # add filepath to filepath.txt for debug
    filepath=os.path.join(saveDir,"filepath.txt")
    helper.writeFile(filepath,filePath)
    f=codecs.open(filePath,"r","utf-8")
    lineNum=0
    classname=""
    while True:
        line=f.readline()
        if line:
            lineNum+=1
            # class
            m=re.match("^\s*class\s+(\w+)\s*\((.*)\)",line)
            if m:
                classname=m.group(1)
                handleClass(saveDir,classname,m.group(2))
                handleDefinition(m.group(1),filePath,lineNum)
                continue
            # function
            m=re.match("^\s*def\s+(\w+)\s*\((.*)\)",line)
            if m:
                handleFunction(saveDir,classname,m.group(1),m.group(2))
                handleDefinition(m.group(1),filePath,lineNum)
                continue
        else:
            break
    f.close()
    
def handleDefinition(keyword,filePath,lineNum):
    global USER_DEFINITIONS
    dot=keyword.rfind(".")
    if dot!=-1:
        keyword2=keyword[(dot+1):]
        handleDefinition(keyword2,filePath,lineNum)
    USER_DEFINITIONS.append([keyword,filePath,filePath,lineNum])

# 处理类
def handleClass(saveDir,classname,params):
    handleFunction(saveDir,"",classname,params)

# 处理函数或方法
def handleFunction(saveDir,classname,funcName,params):
    arr=handleParams(params)
    content=funcName+"(%s)"%(arr[1])
    trigger=funcName+"(%s)"%(arr[0])
    desc="."
    if classname!="":
        desc=classname
    template=snippetTemplate.replace("$content",content)
    template=template.replace("$trigger",trigger)
    template=template.replace("$desc",desc)
    a=""
    if arr[0]!="":
        args=arr[0].split(",")
        for i in range(0,len(args)):
            args[i]=re.sub("\W","",args[i])
        a="_".join(args)
        a="_"+a
    saveName=funcName+a+".sublime-snippet"
    savePath=os.path.join(saveDir,saveName)
    f=open(savePath, "w+")
    f.write(template)
    f.close()

def handleVar(saveDir,varName):
    template=snippetTemplate.replace("$content",varName)
    template=template.replace("$trigger",varName)
    template=template.replace("$desc",".")
    name=re.sub("\W","",varName)
    saveName=name+".sublime-snippet"
    savePath=os.path.join(saveDir,saveName)
    f=open(savePath, "w+")
    f.write(template)
    f.close()

# 处理函数变量,不要self
def handleParams(params):
    args=[]
    for item in params.split(","):
        # 去掉=号后面的
        str1=re.sub("\W","",item.split("=")[0])
        if str1!="" and str1!="self":
            args.append(str1)
    args2=[]
    for i in range(0,len(args)):
        args2.append("${%d:%s}"%(i+1,args[i]))
    a1=""
    a2=""
    if len(args)>0:
        a1=", ".join(args)
        a2=", ".join(args2)
    return [a1,a2]

# delete files under dir
def deleteFiles(path,root):
    if not os.path.exists(path):
        return
    if os.path.isfile(path):
        try:
            os.remove(path)
        except Exception:
            pass
    elif os.path.isdir(path):
        for item in os.listdir(path):
            itemsrc=os.path.join(path,item)
            deleteFiles(itemsrc,root)
        if path!=root:            
            try:
                os.rmdir(path)
            except Exception:
                pass