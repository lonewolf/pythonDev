#pythonDev
###sublime的python开发插件，方便编写python文件。

###安装：
###下载后解压，名字改为pythonDev，放到Packages目录下即可（可能要重启）。

###包含了以下功能和特性：
###1、python2.7的api提示。
###2、在边栏右键建新py文件和__init__.py文件，文件模板可设置username和datetime两个属性，可以配置文件中修改。
###3、生成用户的api提示文件，用法：第一，在边栏右键一个文件夹，点击Rebuild User Definition，这样的话之前所有已生成的用户提示文件都会被删除，再生成新的；第二，保存一个py文件时，会自动生成该文件的api提示，目前支持方法和类。
###4、关键字跳转功能，用法：选择文字后，右键选择Goto Definition，目前支持方法和类关键字，快捷键：ctrl+shift+g。只有执行步骤3才有这个功能。
###5、更正行前的空白字符，python在运行时要求行前面的空白要么是空格要么是制表符，用法：右键点击Correct Indent。
